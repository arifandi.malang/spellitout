﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class basicGameControllerScript : MonoBehaviour
{

    public string[] words = {"DEAF","MUTE"};


    
    

    //test variable
    private GameObject[] btnObject;
    private Button[] btn;
    private GameObject[] btnObjectAns;
    private Button[] btnAns;

    private string testWord = "DEAF";
    public int widthScreen = 1280;
    public int heightScreen = 720;


    //button
    int wordLength = 10;
    private float buttonWidth;
    private float buttonHeight;

    // Start is called before the first frame update
    void Start()
    {

        wordLength = testWord.Length;


        btnObject = new GameObject[wordLength];
        btn = new Button[wordLength];
        btnObjectAns = new GameObject[wordLength];
        btnAns = new Button[wordLength];

        for (int i = 0; i < wordLength; i++) {
            btnObject[i] = GameObject.Find(i.ToString());
            btn[i] = btnObject[i].GetComponent<Button>();

            btnObjectAns[i] = GameObject.Find("a"+i.ToString());
            btnAns[i] = btnObjectAns[i].GetComponent<Button>();
        }
        

        resizeButton();
        moveButton();
        moveAnswer();
    }


    void moveAnswer() {
        float buttonSlotSize = (widthScreen*0.55f) / wordLength;

        for (int i = 0; i < wordLength; i++)
        {
            btnAns[i].image.rectTransform.anchoredPosition = new Vector3( (widthScreen*0.35f)+ (buttonSlotSize / 2) + (buttonSlotSize * i), 250, 0);
        }
    }



    void resizeButton() {

        buttonWidth = widthScreen / wordLength;
        buttonWidth = buttonWidth - (buttonWidth * 0.2f);

        if (buttonWidth > widthScreen / 8) {
            buttonWidth = widthScreen / 8;
        }

        for (int i = 0; i < wordLength; i++)
        {
            btn[i].image.rectTransform.sizeDelta = new Vector2(buttonWidth, buttonWidth);
            btnAns[i].image.rectTransform.sizeDelta = new Vector2(buttonWidth, buttonWidth);
        }
        
    }

    void moveButton() {
        float buttonSlotSize = widthScreen / wordLength;
    
        for (int i = 0; i < wordLength; i++)
        {
            btn[i].image.rectTransform.anchoredPosition = new Vector3((buttonSlotSize/2) + (buttonSlotSize*i), 150, 0);
        }
    }


}
