﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicSoundPlayer : MonoBehaviour
{
    public AudioSource homeSound;
    public AudioSource retrySound;
    public AudioSource hintSound;
   

    // Start is called before the first frame update
    void Start()
    {
        homeSound.GetComponent<AudioSource>();
        retrySound.GetComponent<AudioSource>();
        hintSound.GetComponent<AudioSource>();
    }

    public void buttonSoundPlay(int type)
    {
        switch (type)
        {
            case 0:
                homeSound.Play();
                break;
            case 1:
                retrySound.Play();
                break;
            case 2:
                hintSound.Play();
                break;
        }
    }

   
}
