﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundPlayer : MonoBehaviour
{

    public AudioSource basicSound;
    public AudioSource interSound;
    public AudioSource advanSound;
    // Start is called before the first frame update
    void Start()
    {
        basicSound.GetComponent<AudioSource>();
        interSound.GetComponent<AudioSource>();
        advanSound.GetComponent<AudioSource>();
    }

    public void buttonSoundPlay(int type) {
        switch (type) {
            case 0:
                basicSound.Play();
                break;
            case 1:
                interSound.Play();
                break;
            case 2:
                advanSound.Play();
                break;
        }
    }


}
