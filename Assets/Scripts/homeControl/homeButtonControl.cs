﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class homeButtonControl : MonoBehaviour
{
 
    private float timer;
    private int clickCount;

    int type;
    GameObject soundContainer;
    soundPlayer soundPlayer;

    GameObject goADM;
    answerDetMan answerDetMan;

    public Button[] basicButton;
    private Button[] btn;

    void Start()
    {
        btn = new Button[3];
        //clickCount = new int[3];
        soundContainer = GameObject.Find("soundContainer");
        soundPlayer = soundContainer.GetComponent<soundPlayer>();

        btn[0] = basicButton[0].GetComponent<Button>();
        btn[0].onClick.AddListener(() => TaskOnClick(0));
        btn[1] = basicButton[1].GetComponent<Button>();
        btn[1].onClick.AddListener(() => TaskOnClick(1));
        btn[2] = basicButton[2].GetComponent<Button>();
        btn[2].onClick.AddListener(() => TaskOnClick(2));

        goADM = GameObject.Find("answerDetector");
        answerDetMan = goADM.GetComponent<answerDetMan>();

    }
    private void FixedUpdate()
    {
        timer += Time.deltaTime;

        if (timer > 0.3) {
            if (clickCount == 1)
            {
                Debug.Log("click once" + type);
                soundPlayer.buttonSoundPlay(type);
            }
            else if (clickCount > 1){
                Debug.Log("click twice" + type);
                if (type == 0) {
                    //answerDetMan.point = 3;
                    //answerDetMan.index = 0;
                    answerDetMan.level = 1;
                    if (answerDetMan.wordToPlay == 24)
                    {
                        answerDetMan.completeSound(0);
                    }
                    else {
                        answerDetMan.playScene();
                    }
                    
                }
                else if (type == 1)
                {
                    //answerDetMan.point = 3;
                    //answerDetMan.index = 0;
                    Debug.Log("play Intermediate");
                    answerDetMan.level = 2;
                    if (answerDetMan.wordToPlay1 == 24)
                    {
                        answerDetMan.completeSound(1);
                    }
                    else
                    {
                        answerDetMan.playIntermediate();
                    }

                    
                } else if (type == 2) {
                    answerDetMan.level = 3;
                    

                    if (answerDetMan.wordToPlay2 == 24)
                    {
                        answerDetMan.completeSound(2);
                    }
                    else
                    {
                        answerDetMan.playAdvanced();
                    }
                }
            }
            clickCount = 0;
            timer = 0;
        }
     
    }

    void TaskOnClick(int type)
    {
        this.type = type;
        timer = 0;
        clickCount += 1;
    }
    

}
