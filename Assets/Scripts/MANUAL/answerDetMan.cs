﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class answerDetMan : MonoBehaviour
{
    private string[] word = {"DEAF","BLIND", "AUTISM", "GIFTED", "REGULAR", "RAMP", "WALKER", "BRAILLE", "CANE", "SCREENING", "INCLUSIVE", "BULLYING", "THERAPY", "BASELINE", "EDUCATION", "DISABILITY", "DYSLEXIA", "DYSGRAPHIA", "DYSCALCULIA", "DIVERSITY", "ASSESSMENT", "AWARENESS", "ACCEPTANCE", "HYPERACTIVITY", "EXCEPTIONAL"};
    private string[] word2 = { "MAINSTREAMING", "WHEELCHAIRS", "INTERPRETER", "SEGREGATION", "ACCESSIBLE", "ADAPTATION", "INTERVENTION", "DISCRIMINATION", "ACCELERATION", "MODIFICATION", "CURRICULUM", "MOBILITY", "PARENTING", "VOCATIONAL", "ACCOMODATION", "HANDRAILS", "VISIONLOSS", "CEREBRALPALSY", "DOWNSYNDROME", "SOCIALSKILLS", "SPECIALNEEDS", "FINEMOTOR", "GROSSMOTOR", "SENSORYINTEGRATION", "SIGNLANGUAGE" };
    private string[] word3 = { "LEARNINGDIFFICULTIES", "MOBILITYSKILLS", "SUPPORTASSISTANT", "SENSORYIMPAIRMENT", "SPECIALEDUCATION", "SPEECHDELAY", "DEVELOPMENTALDELAY", "EARLYIDENTIFICATION", "ASSISTIVETECHNOLOGY", "HARDHEARING", "LOWVISION", "ATTENTIONSPAN", "BEHAVIORPROBLEMS", "HEARINGIMPAIRMENT", "VISUALIMPAIRMENT", "TOILETADAPTED", "SUPPORTSERVICES", "EXPERTASSISTANT", "UNIVERSALDESIGN", "SCREENREADER", "HEARINGAIDS", "SPECIALIZEDEQUIPMENT", "DIFFERENTLYABLED", "EDUCATIONALASSISTANT", "ASSISTIVETECHNOLOGY" };
    public char[] myChars;
    public int index;

    public int point;
    public int wordToPlay = 0;
    public int wordToPlay1 = 0;
    public int wordToPlay2 = 0;
    public int level;
    private int[] decklist = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
    private int tempInt;

    private GameObject starAns1;
    private GameObject starAns2;
    private GameObject starAns3;

    public AudioSource basicComplete;
    public AudioSource intermediateComplete;
    public AudioSource advancedComplete;
    

    void Start()
    {
        Shuffle();
        wordToPlay = 0;
        index = 0;
        point = 3;
        level = 0;
        DontDestroyOnLoad(this.gameObject);
    }

    public void testFromDet() {
        Debug.Log("from det");
    }

    public void activateWinUI() {
        starAns1 = GameObject.Find("starScore1");
        starAns2 = GameObject.Find("starScore2");
        starAns3 = GameObject.Find("starScore3");

        if (point == 2) {
            starAns1.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
        } else if (point == 1) {
            starAns2.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
        } else if (point < 1) {
            starAns3.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
        }
    }

    public void playScene() {
        myChars = word[decklist[wordToPlay]].ToCharArray();
        SceneManager.LoadScene(word[decklist[wordToPlay]]);
    }

    public void playIntermediate() {

        myChars = word2[decklist[wordToPlay1]].ToCharArray();
        SceneManager.LoadScene(word2[decklist[wordToPlay1]]);

    }

    public void playAdvanced() {
        myChars = word3[decklist[wordToPlay2]].ToCharArray();
        SceneManager.LoadScene(word3[decklist[wordToPlay2]]);
    }

    public void Shuffle()
    {
        for (int i = 0; i < decklist.Length; i++)
        {
            int rnd = Random.Range(0, decklist.Length);
            tempInt = decklist[rnd];
            decklist[rnd] = decklist[i];
            decklist[i] = tempInt;
        }
    }

    public void completeSound(int type)
    {
        switch (type)
        {
            case 0:
                Debug.Log("copet");
                basicComplete.Play();
                break;
            case 1:
                intermediateComplete.Play();
                break;
            case 2:
                advancedComplete.Play();
                break;
        }
    }

}
