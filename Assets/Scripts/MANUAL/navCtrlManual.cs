﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class navCtrlManual : MonoBehaviour
{
    private GameObject goADM;
    private answerDetMan answerDetMan;

    private float timer;
    private int clickCount;

    public AudioSource hintSound;

    int type;
    GameObject soundContainer;
    basicSoundPlayerManual soundPlayer;

    public Button[] basicButton;
    private Button[] btn;

    void Start()
    {
        btn = new Button[3];
        //clickCount = new int[3];
        soundContainer = GameObject.Find("soundContainer");
        soundPlayer = soundContainer.GetComponent<basicSoundPlayerManual>();

        btn[0] = basicButton[0].GetComponent<Button>();
        btn[0].onClick.AddListener(() => TaskOnClick(0));
        btn[1] = basicButton[1].GetComponent<Button>();
        btn[1].onClick.AddListener(() => TaskOnClick(1));
        btn[2] = basicButton[2].GetComponent<Button>();
        btn[2].onClick.AddListener(() => TaskOnClick(2));

        goADM = GameObject.Find("answerDetector");
        answerDetMan = goADM.GetComponent<answerDetMan>();

    }
    private void FixedUpdate()
    {
        timer += Time.deltaTime;

        if (timer > 0.3)
        {
            if (clickCount == 1)
            {
                Debug.Log("click once" + type);
                soundPlayer.buttonSoundPlay(type);
            }
            else if (clickCount > 1)
            {
                Debug.Log("click twice" + type);

                if (type == 0) {
                    answerDetMan.index = 0;
                    answerDetMan.point = 3;
                    SceneManager.LoadScene("homeScene");
                }
                else if (type == 1)
                {
                    Scene scene = SceneManager.GetActiveScene();
                    SceneManager.LoadScene(scene.name);
                    answerDetMan.index = 0;
                    answerDetMan.point = 3;
                }
                else if (type == 2) {
                    Debug.Log("play hint");
                    hintSound.Play();
                }
            }
            clickCount = 0;
            timer = 0;
        }

    }

    void TaskOnClick(int type)
    {
        this.type = type;
        timer = 0;
        clickCount += 1;
    }

}
