﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicSoundPlayerManual : MonoBehaviour
{
    public AudioSource homeSound;
    public AudioSource retrySound;
    public AudioSource hintSound;
    public AudioSource wrongSound;
    public AudioSource trueSound;
    public AudioSource winSound;
    

    // Start is called before the first frame update
    void Start()
    {
        homeSound.GetComponent<AudioSource>();
        retrySound.GetComponent<AudioSource>();
        hintSound.GetComponent<AudioSource>();
        wrongSound.GetComponent<AudioSource>();
        winSound.GetComponent<AudioSource>();
        trueSound.GetComponent<AudioSource>();
    }

    public void buttonSoundPlay(int type)
    {
        switch (type)
        {
            case 0:
                homeSound.Play();
                break;
            case 1:
                retrySound.Play();
                break;
            case 2:
                hintSound.Play();
                break;
        }
    }

    public void playWrong() {
        wrongSound.Play();
    }

    public void playWin() {
        winSound.Play();
    }
    public void playTrue() {
        trueSound.Play();
    }

}
