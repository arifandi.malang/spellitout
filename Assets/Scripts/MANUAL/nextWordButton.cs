﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class nextWordButton : MonoBehaviour
{
    GameObject goADM;
    answerDetMan answerDetMan;

    GameObject goBasicSoundPlayer;
    basicSoundPlayer basicSoundPlayer;

    GameObject sliderLoading;
    GameObject nameScore;
    GameObject wordComplete;
    
    private float timer;
    private float sliderTimer;
    private float timerToHome;

    private int playWinSound;
    private int playRankSound;
    private int returnToHomeBool;
    private int playComplete;

    public AudioSource excellent;
    public AudioSource veryGood;
    public AudioSource good;
    public AudioSource fair;
    public AudioSource winsound;
    
    // Start is called before the first frame update
    void Start()
    {
        timerToHome = 10;
        timer = 0;
        playWinSound = 0;
        playComplete = 0;
        playRankSound = 0;
        goADM = GameObject.Find("answerDetector");
        answerDetMan = goADM.GetComponent<answerDetMan>();



        sliderLoading = GameObject.Find("loadingSlide");
        nameScore = GameObject.Find("nameScore");
        wordComplete = GameObject.Find("wordComplete");
        winsound.Play();
      //  changeRank();

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        sliderTimer += Time.deltaTime;
        timerToHome += Time.deltaTime;


        if (sliderTimer > 0.1) {
            sliderLoading.GetComponent<Slider>().value -= 10;
            sliderTimer = 0;
        }
        
        if (timer >= 2.5) {
            nextWord();
        }
        else if (timer < 2.5)
        {           
            changeRank();
        }

        if (timer >= 1.5f) {
            winsound.Stop();
        }

        if (timerToHome > 3f && timerToHome < 4f) {
            returnToHome();
        }
        
    }

    public void changeWordComplete() {


    }

    public void changeRank() {

        if (answerDetMan.point == 3)
        {
            nameScore.GetComponent<Text>().text = "EXCELLENT";
            if (!winsound.isPlaying)
            {
                if (!excellent.isPlaying)
                {
                    if (playRankSound == 0) {
                        excellent.Play();
                        playRankSound = 1;
                    }
                }  
            }
        }
        else if (answerDetMan.point == 2)
        {
            nameScore.GetComponent<Text>().text = "VERY GOOD";
            if (!winsound.isPlaying)
            {
                if (!excellent.isPlaying)
                {
                    if (playRankSound == 0)
                    {
                       veryGood.Play();
                        playRankSound = 1;
                    }
                }
            }
        }
        else if (answerDetMan.point == 1)
        {
            nameScore.GetComponent<Text>().text = "GOOD";

            if (!winsound.isPlaying)
            {
                if (!excellent.isPlaying)
                {
                    if (playRankSound == 0)
                    {
                        good.Play();
                        playRankSound = 1;
                    }
                }
            }

        }
        else if (answerDetMan.point < 1)
        {
            nameScore.GetComponent<Text>().text = "FAIR";
            if (!winsound.isPlaying)
            {
                if (!excellent.isPlaying)
                {
                    if (playRankSound == 0)
                    {
                       fair.Play();
                        playRankSound = 1;
                    }
                }
            }
        }

    }

    public void returnToHome() {
        answerDetMan.index = 0;
        answerDetMan.point = 3;
        SceneManager.LoadScene("homeScene");
        //answerDetMan.playScene();
    }

    public void nextWord() {
        //answerDetMan.wordToPlay = 24;
        //answerDetMan.wordToPlay1 = 24;
        //answerDetMan.wordToPlay2 = 24;
        answerDetMan.index = 0;
        answerDetMan.point = 3;
        Debug.Log("win nex word" + answerDetMan.wordToPlay);
        if (answerDetMan.level == 1) {

            if (answerDetMan.wordToPlay == 24)
            {
                
                if (playComplete == 0) {
                    answerDetMan.completeSound(0);
                    playComplete = 1;
                    timerToHome = 0;
                }
                
                
            }
            else {
                answerDetMan.playScene();
            }
        } else if (answerDetMan.level == 2) {

            if (answerDetMan.wordToPlay1 == 24)
            {
                if (playComplete == 0)
                {
                    answerDetMan.completeSound(1);
                    playComplete = 1;
                    timerToHome = 0;
                }
            }
            else
            {
                answerDetMan.playIntermediate();
            }
            
        } else if (answerDetMan.level == 3) {
            if (answerDetMan.wordToPlay2 == 24)
            {
                if (playComplete == 0)
                {
                    answerDetMan.completeSound(2);
                    playComplete = 1;
                    timerToHome = 0;
                }
            }
            else
            {
                answerDetMan.playAdvanced();
            }
            
        }


        
    }

}
