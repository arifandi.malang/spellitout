﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreCounter : MonoBehaviour
{

    private GameObject goADM;
    private answerDetMan answerDetMan;
    private GameObject star1;
    private GameObject star2;
    private GameObject star3;
    
    // Start is called before the first frame update
    void Start()
    {
        goADM = GameObject.Find("answerDetector");
        answerDetMan = goADM.GetComponent<answerDetMan>();
        star1 = GameObject.Find("star1");
        star2 = GameObject.Find("star2");
        star3 = GameObject.Find("star3");
        
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("score now : "+ answerDetMan.point);

        if (answerDetMan.point == 2)
        {
            //star3.SetActive(false);
            star1.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);


        }
        else if (answerDetMan.point == 1)
        {
            //star2.SetActive(false);
            star1.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
            star2.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
        }
        else if (answerDetMan.point <= 0)
        {
            //star1.SetActive(false);
            star1.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
            star2.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
            star3.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f);
        }
        
    }
}
