﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonChoose : MonoBehaviour
{
    private float timer;

    int switchable = 1;

    private int clickCount;

    private GameObject goADM;
    private answerDetMan answerDetMan;

    private GameObject goSwitcher;
    private switcher switcher;

    private GameObject soundContainer;
    private basicSoundPlayerManual basicSoundPlayerManual;

    public char[] myChars;
    private string letter;
    int index;
    

    // Start is called before the first frame update
    void Start()
    {
        goADM = GameObject.Find("answerDetector");
        answerDetMan = goADM.GetComponent<answerDetMan>();

        goSwitcher = GameObject.Find("switcher");
        switcher = goSwitcher.GetComponent<switcher>();

        soundContainer = GameObject.Find("soundContainer");
        basicSoundPlayerManual = soundContainer.GetComponent<basicSoundPlayerManual>();

        
    }

    private void FixedUpdate()
    {
      //  Debug.Log(this.transform.position);
        timer += Time.deltaTime;
        
        if (timer > 0.3)
        {
            if (clickCount == 1)
            {
                Debug.Log("click once" );
                this.GetComponent<AudioSource>().Play();
            }
            else if (clickCount > 1 && switchable == 1)
            {
                //this.GetComponent<AudioSource>().Stop();
                Debug.Log("click twice" );
                clickThisButtonTwice();
            }
            clickCount = 0;
            timer = 0;
        }

    }

    public void clickThisButton() {
  
        timer = 0;
        clickCount += 1;
    }

    public void clickThisButtonTwice() {

        Vector3 thisButtonPos = this.transform.position;
        
        letter = this.GetComponentInChildren<Text>().text;
        index = answerDetMan.index;

        if (letter == answerDetMan.myChars[index].ToString())
        {
            switchable = 0;
            basicSoundPlayerManual.playTrue();
            switcher.switchTrueLetter(index, thisButtonPos);
            this.transform.position = switcher.getEmpPos();
            answerDetMan.index++;
        }
        else {
            answerDetMan.point -= 1;
            basicSoundPlayerManual.playWrong();
        }

        if (answerDetMan.index == answerDetMan.myChars.Length) {
            Debug.Log("win");
            //basicSoundPlayerManual.playWin();
            
            if (answerDetMan.level == 1) {
                answerDetMan.wordToPlay++;
                //if (answerDetMan.wordToPlay == 25) {
                //    answerDetMan.level = 2;
                //}
            }
            else if (answerDetMan.level == 2)
            {
                answerDetMan.wordToPlay1++;
                 //   answerDetMan.level = 3;
            }
            else if (answerDetMan.level == 3)
            {
                answerDetMan.wordToPlay2++;
            }

            SceneManager.LoadScene("winUI");
            
        }

    }
}
